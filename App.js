import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Navigator from './src/Navigation/Navigator';


export default function App() {
  return (
    <View style={styles.container}>
      <Navigator/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  txtstlye:{
    color:"blue",
    fontSize:16,
    justifyContent:'center',
    alignSelf:'center'

  },
  touchablestyle:{
    backgroundColor:"blue",
    justifyContent:'center',
    width:"60%",
    alignSelf:'center',
    marginTop:20,
    height:35
  },
  touchabletxt:{
    fontSize:13,
    color:"white",
    textAlign:'center'
  }
});

import { useLinkProps } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet, Text, View ,Image,FlatList} from 'react-native';
import { SearchBar } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';

const StudentList=(props)=> {
 const [search,setsearch]=useState("")

  const [arrstudents,setarrstudents]=useState(
    [

    {
      Name:"Abhay",
     id:0,
     image:"http://www.spica-siam.com/wp-content/uploads/2017/12/user-demo.png" ,
     
  },
  {
    Name:"Chandani",
   id:1,
   image:"http://www.spica-siam.com/wp-content/uploads/2017/12/user-demo.png" ,
},
{
  Name:"Doll",
 id:2,
 image:"http://www.spica-siam.com/wp-content/uploads/2017/12/user-demo.png" ,
},
{
  Name:"Himanshi",
 id:3,
 image:"http://www.spica-siam.com/wp-content/uploads/2017/12/user-demo.png" ,
},
{
  Name:"Gopal",
 id:4,
 image:"http://www.spica-siam.com/wp-content/uploads/2017/12/user-demo.png" ,
},

  ])
  Methoddelete=(index)=>{
     console.log(index)
    let news=arrstudents.filter((item)=>item.id !== index)
    setarrstudents(news)
    console.log(news,arrstudents)
  }
  searchFilterFunction = text => {    
    const newData = arrstudents.filter(item => {   
      console.log("item",item)   
      const itemData = `${item.Name}   `;
      
       const textData = text;
        console.log("--",itemData,textData)
        
       return itemData.indexOf(textData) > -1;    
    });
    setarrstudents(newData)
    setsearch(text)
      
  };
  renderCell=(item)=>{
    const index = arrstudents.indexOf(item.item);
  return(
    <View style={{marginLeft:20}}>


  <View style={{flexDirection:"row",margin:6,width:"100%"}}>
        <View style={{width:"50%",flexDirection:"row"}}>
      <Image style={{width:50,height:50}} source={{uri:item.item.image}}/>
      <View style={{justifyContent:'center'}}>
       <Text style={styles.txtrender}>{item.item.Name}</Text>
       </View>
       </View>
       <View style={{flexDirection:'row'}}>
       <View style={{justifyContent:'center'}}>
       <TouchableOpacity onPress={()=>props.navigation.navigate("EditStudents",{onNavigateBackEdit:(data,id)=>handleOnNavigateBackEdit(data,id),dicData:item.item})}>
       <Text style={styles.txtrender}>Edit</Text>
       </TouchableOpacity>
       </View>
       <View style={{justifyContent:'center'}}>
       <TouchableOpacity onPress={()=>Methoddelete(item.item.id)}>
       <Text style={styles.txtrender}>Delete</Text>
       </TouchableOpacity>
       </View>
       </View>
       </View>
      
       </View>
  )
  }
  MethodSort=()=>{
   
    var arr=arrstudents;
    arr.sort((a,b)=>
  ( a.Name-b.Name)?1:-1
    )
  
    setarrstudents(arr)
    console.log("arr",arr,arrstudents)

  }
  MethodAdd=()=>{
    console.log("navigation")
    props.navigation.navigate('AddStudents',{onNavigateBack:(data)=>handleOnNavigateBack(data),dicData:arrstudents,id:arrstudents.length})
  }
  handleOnNavigateBack = (dat) => {
    console.log("data",dat)
    let arr=arrstudents;
    arr.push(dat)
    console.log(arr,arrstudents)
    setarrstudents(arr)
  }
  handleOnNavigateBackEdit = (dat,index) => {
    console.log("data",dat,index)
    let newarrstudents = [ ...arrstudents ];
    newarrstudents[index].Name = dat;
    console.log("new",newarrstudents[index])
   setarrstudents(newarrstudents)
    
  }
  return (
    <View style={styles.container}>
      
      <SearchBar        
      placeholder="Type Here..."        
      lightTheme        
      round        
      value={search}
      onChangeText={(text) => searchFilterFunction(text)}
      autoCorrect={false}             
    />   
       <Text style={styles.touchabletxt}> Students List</Text>
     <FlatList
     data={arrstudents}
     style={{height:40,flex:0.5}}
     renderItem={renderCell}
     />

  <TouchableOpacity onPress={()=>MethodAdd()} style={styles.touchablestyle}>
       <Text style={styles.touchabletxt}>Add</Text>
       </TouchableOpacity>
       
  <TouchableOpacity onPress={()=>MethodSort()} style={styles.touchablestyle}>
       <Text style={styles.touchabletxt}>Sort</Text>
       </TouchableOpacity>
    </View>

    

  );
}
const styles = StyleSheet.create({
  txtrender:{
   fontSize:12,
   color:"black",
   marginLeft:15,
   textAlign:'center'
  },
  container: {
    flex:0.9,
    
    backgroundColor: '#fff',
  },
  txtstlye:{
    color:"blue",
    fontSize:16,
    justifyContent:'center',
    alignSelf:'center'

  },
  touchablestyle:{
    backgroundColor:"blue",
    justifyContent:'center',
    width:"60%",
    alignSelf:'center',
    marginTop:20,
    height:35
  },
  touchabletxt:{
    fontSize:13,
    color:"white",
    textAlign:'center'
  }
});

export default StudentList;
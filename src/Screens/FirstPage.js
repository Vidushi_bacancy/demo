import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const firstPage=(props)=> {
  return (
    <View style={styles.container}>
      <View style={{marginTop:10,}}>
      <Text style={styles.txtstlye}>Demo Project </Text>
      </View>

      <TouchableOpacity onPress={()=>props.navigation.navigate("StudentsList")} style={styles.touchablestyle}>
        <Text style={styles.touchabletxt}> Students List</Text>
      </TouchableOpacity>
      {/* <TouchableOpacity style={styles.touchablestyle}>
      
        <Text style={styles.touchabletxt}> Add Students </Text>
      </TouchableOpacity> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  txtstlye:{
    color:"black",
    fontSize:16,
    justifyContent:'center',
    alignSelf:'center',
    fontWeight:'bold'

  },
  touchablestyle:{
    backgroundColor:"blue",
    justifyContent:'center',
    width:"60%",
    alignSelf:'center',
    marginTop:20,
    height:35
  },
  touchabletxt:{
    fontSize:13,
    color:"white",
    textAlign:'center'
  }
});

export default firstPage;
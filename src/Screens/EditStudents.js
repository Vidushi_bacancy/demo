import { useLinkProps } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet, Text, TextInput,View ,Image,FlatList} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import StudentList from './StudentsList';

const EditStudents=(props)=> {
console.log(props.route)
  const [studentname,setstudentname]=useState(props.route.params.dicData.Name)
  const [id,setid]=useState(props.route.params.dicData.id)
 


  Methodedit=()=>{
      console.log(studentname,id)
      props.route.params.onNavigateBackEdit(studentname,id)
      props.navigation.goBack()
  }
  return (
    <View style={styles.container}>
       <Text style={styles.touchabletxt}> Add Students </Text>
       <View style={{width:"80%",marginLeft:8}}>
    <TextInput
    placeholder="Student Name"
    value={studentname}
    onChangeText={(item)=>setstudentname(item)}
    style={{borderBottomWidth:1,paddingBottom:0}}
    />
</View>
<View style={{marginTop:9}}>
  <TouchableOpacity onPress={()=>Methodedit()} style={styles.touchablestyle}>
       <Text style={styles.touchabletxt}>Done</Text>
       </TouchableOpacity>
       </View>
      
    </View>
    

  );
}
const styles = StyleSheet.create({
    touchabletxt:{
        fontSize:13,
        color:"white",
        textAlign:'center'
      },
  txtrender:{
   fontSize:12,
   color:"black",
   marginLeft:15,
   textAlign:'center'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  txtstlye:{
    color:"blue",
    fontSize:16,
    justifyContent:'center',
    alignSelf:'center'

  },
  touchablestyle:{
    backgroundColor:"blue",
    justifyContent:'center',
    width:"60%",
    alignSelf:'center',
    marginTop:20,
    height:35
  },
  touchabletxt:{
    fontSize:13,
    color:"white",
    textAlign:'center'
  }
});

export default EditStudents;
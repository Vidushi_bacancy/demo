import { useLinkProps } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet, Text, TextInput,View ,Image,FlatList} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import StudentList from './StudentsList';

const AddStudents=(props)=> {
console.log(props.route.params.dicData)
  const [studentname,setstudentname]=useState("")
  const [id,setid]=useState(props.route.params.id)


  MethodAdd=()=>{
      let ID=id;
      console.log("id",ID,ID++)
      let dicData={Name:studentname,id:ID++,image:"http://www.spica-siam.com/wp-content/uploads/2017/12/user-demo.png" ,}
      props.route.params.onNavigateBack(dicData)
      props.navigation.goBack()
     
  }
  return (
    <View style={styles.container}>
       <Text style={styles.touchabletxt}> Add Students </Text>
       <View style={{width:"80%",marginLeft:8}}>
    <TextInput
    placeholder="Student Name"
    value={studentname}
    onChangeText={(item)=>setstudentname(item)}
    style={{borderBottomWidth:1,paddingBottom:0}}
    />
</View>
<View style={{marginTop:9}}>
  <TouchableOpacity onPress={()=>MethodAdd()} style={styles.touchablestyle}>
       <Text style={styles.touchabletxt}>Done</Text>
       </TouchableOpacity>
       </View>
    </View>

  );
}
const styles = StyleSheet.create({
  txtrender:{
   fontSize:12,
   color:"black",
   marginLeft:15,
   textAlign:'center'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  txtstlye:{
    color:"blue",
    fontSize:16,
    justifyContent:'center',
    alignSelf:'center'

  },
  touchablestyle:{
    backgroundColor:"blue",
    justifyContent:'center',
    width:"60%",
    alignSelf:'center',
    marginTop:20,
    height:35
  },
  touchabletxt:{
    fontSize:13,
    color:"white",
    textAlign:'center'
  }
});

export default AddStudents;
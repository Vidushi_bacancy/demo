import {createStackNavigator} from '@react-navigation/stack'
import firstPage from "../Screens/FirstPage";
import React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import StudentsList from '../Screens/StudentsList';
import AddStudents from '../Screens/AddStudents';
import EditStudents from '../Screens/EditStudents';
const DemoStack=createStackNavigator();
const StackStudent=({ navigation})=>(
    <DemoStack.Navigator>
        <DemoStack.Screen name="DemoProject" component={firstPage} options={{headershown:false}}/>
        <DemoStack.Screen name="StudentsList" component={StudentsList} options={{headershown:false}}/>
        <DemoStack.Screen name="AddStudents" component={AddStudents} options={{headershown:false}}/>
        <DemoStack.Screen name="EditStudents" component={EditStudents} options={{headershown:false}}/>
    </DemoStack.Navigator>
)

export default ()=>{
return(
    <NavigationContainer>
        <StackStudent/>
    </NavigationContainer>
)
}